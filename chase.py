import random
BATSMAN = 0
HITTER_BATSMAN = 1
DEFENSIVE_BATSMAN = 2
BOWLER = 3

SAMPLE_BATSMAN = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
SAMPLE_HITTER_BATSMAN = [0, 4, 6]
SAMPLE_DEFENSIVE = [0, 1, 2, 3]
SAMPLE_BOWLER = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

TYPE_SAMPLE_MAP = {
    BATSMAN: SAMPLE_BATSMAN,
    HITTER_BATSMAN: SAMPLE_HITTER_BATSMAN,
    DEFENSIVE_BATSMAN: SAMPLE_DEFENSIVE,
    BOWLER: SAMPLE_BOWLER
}

class Game:
    def __init__(self, overs, target, batsman, bowler):
        self.batsman = batsman
        self.bowler = bowler
        self.target = target
        self.overs = overs
        self.deliveries = self.overs * 6

    
    def move(self):
        batsman_score = self.batsman.score()
        bowler_score = self.bowler.score()

        print("Batsman score: {}".format(batsman_score))
        print("Bowler score: {}".format(bowler_score))
        print("-" * 20)
        return batsman_score, bowler_score
    

    def play(self):
        for i in range(self.deliveries):
            batsman_score, bowler_score = self.move()

            if batsman_score == bowler_score:
                self.batsman.discard_score()
                break
        
        if self.batsman.total_score > self.target:
            print("Batsman has won!")
        else:
            print("Bowler has won!")



class Player:
    def __init__(self, player_type):
        self.player_type = player_type
        self.sample = TYPE_SAMPLE_MAP[self.player_type]
        self.random = random
        self.total_score = 0
        self.prev_score = None
        self.runs = None

    def score(self):
        self.prev_score = self.runs
        self.runs = self.random.sample(self.sample, 1)[0]
        self.total_score += self.runs
        return self.runs
    
    def discard_score(self):
        self.total_score -= self.runs
        self.runs = self.prev_score
        

if __name__ == "__main__":
    overs = 2
    target = 25
    player_type = int(input("Enter batsman type. 0. Normal batsman 1. Hitter. 2. Defensive : "))
    batsman = Player(player_type)
    bowler = Player(BOWLER)
    game = Game(overs, target, batsman, bowler)
    game.play()


